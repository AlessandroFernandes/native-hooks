import React, { Fragment, useEffect, useState } from 'react';
import { FlatList, ScrollView } from 'react-native';
import Cabecalho from './components/Cabecalho/Cabecalho';
import Fotos from './components/Fotos/Fotos';
import BuscarFotos from './services/apiDefault';
import Comentario from './components/Comentario/Comentario';

export default function App() {

  const [ foto, setFoto ] = useState([]);

  useEffect(() => { BuscarFotos(setFoto) }, []);

  return (
    <ScrollView>
      <FlatList 
        data={ foto }
        keyExtractor={(item) => item.id.toString() }
        renderItem={ ({ item }) => 
        <Fragment>
          <Cabecalho  
            itemTitulo={ item.userName }
            urlFoto={ item.userURL }
          />
          <Fotos
            foto={ item.url }
            descricao={ item.description }
            likes={ item.likes }
          />
          <Comentario 
            itemComantario={ item.comentarios }
          />
        </Fragment>    
        }
      />
    </ScrollView>
  )
}
