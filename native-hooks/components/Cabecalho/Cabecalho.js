import React, { Fragment } from 'react';
import { Text,
         Image,
         View } from 'react-native';
import Estilo from './estilo';
export default Cabecalho = ({ itemTitulo, urlFoto }) => {
    return (
        <Fragment>
            <View style={ Estilo.view }>
                <Image 
                    source={{ uri: urlFoto }}
                    style={ Estilo.imagem }
                />
                <Text>{ itemTitulo }</Text>
            </View>
        </Fragment>
    )
}
