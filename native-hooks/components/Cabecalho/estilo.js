import { StyleSheet } from 'react-native';

export default Estilo = StyleSheet.create({
    imagem: {
        width: 40,
        height: 40,
        borderRadius: 30,
        marginRight: 10
    },
    view : {
        flexDirection: 'row',
        margin: 7,
        alignItems: 'center'
    }
})