import { StyleSheet } from 'react-native';

const Estilo = StyleSheet.create({
    comentario: {
        marginBottom: 5,
    },
    foto: {
        height: 20,
        width: 20,
        marginRight: 5
    },
    comentario__bloco: {
        flexDirection: 'row',
        alignItems: 'center'
    }
});

export default Estilo;