import React, { Fragment, useState, ref } from 'react';
import { Text, FlatList, TextInput, Image, TouchableOpacity, View } from 'react-native';
import Estilo from './estilo';

export default Comentario = ({ itemComantario}) => {
    const [ estComentario, setEstComentario ] = useState(itemComantario);

    let refComentario;
    let textoComentario = '';

    const envioComentario = () => {
        refComentario.clear();
        const novoComentario =  {
                                    date: Date.now(),
                                    text: textoComentario,
                                    userName: "Alessandro"
                                };
        setEstComentario([...estComentario, novoComentario])
    }

    return (
        <Fragment>
            <FlatList 
                data={ estComentario }
                keyExtractor={(item, index) => index.toString() }
                renderItem={ ({ item }) => 
                    <Fragment>
                        <Text 
                            style={ Estilo.comentario }
                        >
                            {item.userName}: {item.text}
                        </Text>
                    </Fragment>
                }
            />
            <View style={ Estilo.comentario__bloco }>
                <TextInput
                    ref={ texto => refComentario = texto }
                    onChangeText={text => textoComentario = text}
                    placeholder={"Digite seu comentario aqui..."}
                    style={{ flex: 1, marginBottom: 10 }}
                />
                <TouchableOpacity 
                    onPress={ envioComentario }
                >
                    <Image 
                        source={require("../../assets/send.png")}
                        style={ Estilo.foto }
                    />
                </TouchableOpacity>
            </View>
        </Fragment>
    )
}
