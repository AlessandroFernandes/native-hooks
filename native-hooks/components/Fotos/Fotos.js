import React, { Fragment, useState } from 'react';
import { Image, Text, View, TouchableOpacity } from 'react-native';
import { ImageLike, HandleCurtida } from './LikeService';
import Estilo from './estilo';

const Fotos = ({ foto, likes }) => {

    const[like, setLike] = useState(likes);
    const[curtiu, setCurtiu] = useState(false);

    const handleCurtida = () => {
        const [ curtida, likes ] = HandleCurtida(curtiu, like)
        setLike(likes);
        setCurtiu(!curtida)
    }

    return(
    <Fragment>
        <Image 
            source={{ uri: foto }}
            style={ Estilo.imagem }
        />
        <View
            style={ Estilo.curtidas }
        >
            <TouchableOpacity onPress={ handleCurtida }>
                <Image 
                    source={ ImageLike(curtiu) } 
                    style={ Estilo.like }
                />
            </TouchableOpacity>
            <Text>{ like }</Text>
        </View>
    </Fragment>
    )
}

export default Fotos;