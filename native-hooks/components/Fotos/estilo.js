import { StyleSheet, Dimensions } from 'react-native';

const tela = Dimensions.get("screen").width;

const Estilo = StyleSheet.create({
    imagem: {
      width:  tela,
      height: tela,
    },
    description: {
      margin: 5,
      marginBottom: 10
    },
    like: {
      width: 40,
      height: 40,
      margin: 5
    },
    curtidas: {
     flexDirection: 'row',
     alignItems: 'center'
    }
})

export default Estilo;